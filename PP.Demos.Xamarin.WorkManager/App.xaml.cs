﻿using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PP.Demos.Xamarin.WorkManager
{
    public partial class App : Application
    {
        //IWorker Worker;
        public App()
        {
            InitializeComponent();            
            //this.Worker = DependencyService.Get<IWorker>();
            MainPage = new MainPage();
        }

        //protected override void OnStart()
        //{
        //    // Handle when your app starts
        //}

        //protected override void OnSleep()
        //{
        //    if (this.Worker != null)
        //        this.Worker.StartWork();
        //    else
        //        Debugger.Break();
        //}

        //protected override void OnResume()
        //{
        //    if (this.Worker != null)
        //        this.Worker.CancelWork();
        //    else
        //        Debugger.Break();
        //}
    }
}
