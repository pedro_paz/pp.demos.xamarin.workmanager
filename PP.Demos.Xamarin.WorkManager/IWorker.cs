﻿using System;
namespace PP.Demos.Xamarin.WorkManager
{
    public interface IWorker
    {
        void StartWork();        
    }
}
