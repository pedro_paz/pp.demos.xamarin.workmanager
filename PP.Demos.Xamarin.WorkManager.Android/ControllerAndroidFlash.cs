﻿using System;
using Xamarin.Forms;
using PP.Demos.Xamarin.WorkManager.Droid;
using Android.Hardware;
using static Android.Hardware.Camera;
using Android.Support.V4.Content;
using Android;
using Android.Support.V4.App;
using Plugin.CurrentActivity;
using Android.Content.PM;
using Android.Content;

[assembly: Dependency(typeof(ControllerAndroidFlash))]
namespace PP.Demos.Xamarin.WorkManager.Droid
{
    public class ControllerAndroidFlash 
    {
        private Camera Camera;
        private Parameters CameraParams;
        public bool TurnedOn
        {
            get;
            set;
        }

        public ControllerAndroidFlash(Context context)
        {
            this.TurnedOn = false;

            if (ContextCompat.CheckSelfPermission(context, Manifest.Permission.Camera) != Permission.Granted) throw new Exception("Permissao nao concedida");
            if (!context.PackageManager.HasSystemFeature(PackageManager.FeatureCamera)) throw new Exception("Dispositivo nao possui camera");
            if (!context.PackageManager.HasSystemFeature(PackageManager.FeatureCameraFlash)) throw new Exception("Dispositivo nao possui flash");


            this.Camera = Camera.Open();
            this.CameraParams = this.Camera.GetParameters();
        }


        public ControllerAndroidFlash()
        {
            this.TurnedOn = false;

            if (ContextCompat.CheckSelfPermission(CrossCurrentActivity.Current.Activity, Manifest.Permission.Camera) != Permission.Granted) throw new Exception("Permissao nao concedida");
            if (!CrossCurrentActivity.Current.Activity.PackageManager.HasSystemFeature(PackageManager.FeatureCamera)) throw new Exception("Dispositivo nao possui camera");
            if (!CrossCurrentActivity.Current.Activity.PackageManager.HasSystemFeature(PackageManager.FeatureCameraFlash)) throw new Exception("Dispositivo nao possui flash");


            this.Camera = Camera.Open();
            this.CameraParams = this.Camera.GetParameters();
        }



        public void ToggleFlash()
        {
            if (this.Camera == null) this.Camera = Camera.Open();

            if (this.CameraParams == null) this.CameraParams = this.Camera.GetParameters();

            if (this.Camera == null) throw new Exception("Nao foi possivel recuperar `Camera`");
            if (this.CameraParams == null) throw new Exception("Nao foi possivel recuperar `CameraParams`");

            try
            {
                this.CameraParams = this.Camera.GetParameters();
            }
            catch (Exception)
            {

            }

            this.CameraParams.FlashMode = this.TurnedOn ? Parameters.FlashModeOff : Parameters.FlashModeTorch;
            this.Camera.SetParameters(this.CameraParams);
            this.Camera.StartPreview();

            this.TurnedOn = !this.TurnedOn;
        }
    }
}
