﻿using System;
using System.Diagnostics;
using Android.App;
using Android.Content;
using Android.Support.V4.App;
using Plugin.CurrentActivity;

namespace PP.Demos.Xamarin.WorkManager.Droid
{

    
    [BroadcastReceiver(Enabled = true)]
    public class AlarmManagerReceiver : BroadcastReceiver
    {

        static readonly int NOTIFICATION_ID = 1000;
        static readonly string CHANNEL_ID = "location_notification";

        public override void OnReceive(Context context, Intent intent)
        {    
            this.ShowNotification(context);

        }
       
        private void ShowNotification(Context context)
        {

            // Build the notification:
            var builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                          .SetAutoCancel(true) // Dismiss the notification from the notification area when the user clicks on it                          
                          .SetContentTitle("Alarme: " + DateTime.Now.ToString("HH:mm:ss")) // Set the title                          
                          .SetSmallIcon(Resource.Drawable.icon);// This is the icon to display


            // Finally, publish the notification:
            var notificationManager = NotificationManagerCompat.From(context);
            notificationManager.Notify(NOTIFICATION_ID, builder.Build());



        }
    }
   

}

