﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;

namespace PP.Demos.Xamarin.WorkManager.Droid
{
    public class CurrentActivity : Application, Application.IActivityLifecycleCallbacks
    {
        internal static Activity CurrentContext { get; private set; }

        public CurrentActivity(IntPtr handle, JniHandleOwnership transfer) :
          base(handle, transfer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            RegisterActivityLifecycleCallbacks(this);
        }

        public override void OnTerminate()
        {
            base.OnTerminate();
            UnregisterActivityLifecycleCallbacks(this);
        }

        public void OnActivityCreated(Activity activity, Bundle savedInstanceState) => CurrentContext = activity;

        public void OnActivityDestroyed(Activity activity) { }

        public void OnActivityPaused(Activity activity) { }

        public void OnActivityResumed(Activity activity) => CurrentContext = activity;

        public void OnActivitySaveInstanceState(Activity activity, Bundle outState) { }

        public void OnActivityStarted(Activity activity) => CurrentContext = activity;

        public void OnActivityStopped(Activity activity) { }

    }
}
