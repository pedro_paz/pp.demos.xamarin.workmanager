﻿using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using Xamarin.Essentials;
using Android.Content;

namespace PP.Demos.Xamarin.WorkManager.Droid
{
    [Activity(Label = "PP.Demos.Xamarin.WorkManager", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {

        static readonly int NOTIFICATION_ID = 1000;
        static readonly string CHANNEL_ID = "location_notification";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);


            //< Target Name = "FixGuava" AfterTargets = "_AddAndroidCustomMetaData" >   
            //   < ItemGroup >   
            //       <_MonoAndroidReferencePath Include = "@(_ReferencePath);@(_ReferenceDependencyPaths)" Condition = " '%(Filename)' == 'Xamarin.Google.Guava.ListenableFuture' " />      
            //   </ ItemGroup >      
            //</ Target >

            ///-----------------------------------------------------------------------------//
            ///-----------------------------------------------------------------------------//
            ///-----------------------------------------------------------------------------//

            Platform.Init(this, savedInstanceState);


            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());

            CreateNotificationChannel();            
            StartBroadcast();


        }

        private void StartBroadcast()
        {
                
            this.StopService(new Intent(this, typeof(AlarmManagerReceiver)));

            AlarmManager manager = (AlarmManager)GetSystemService(AlarmService);
            Intent minhaIntent;
            PendingIntent pendingIntent;

            

            minhaIntent = new Intent(this, typeof(AlarmManagerReceiver));

            pendingIntent = PendingIntent.GetBroadcast(this, 0, minhaIntent, 0);
            manager.SetInexactRepeating(AlarmType.RtcWakeup, SystemClock.ElapsedRealtime() + 5000, 60000, pendingIntent);


        }


        private void CreateNotificationChannel()
        {

            var name = "Teste Alarm Manager";
            var description = "Este é um teste do projeto PP.Demos.Xamarin.WorkManager";
            var channel = new NotificationChannel(CHANNEL_ID, name, NotificationImportance.Default)
            {
                Description = description
            };

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }

     
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);        
        }
    }
}
